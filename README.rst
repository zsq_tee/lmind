Installing
----------

Install and update using `pip`_:

.. code-block:: text

    $ pip install lmind

.. _pip: https://pip.pypa.io/en/stable/quickstart/


Links
-----

-   Documentation: https://gitee.com/zsq_tee/lmind
-   Source Code: https://gitee.com/zsq_tee/lmind
