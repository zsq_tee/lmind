# lmind

#### Description
This is a simple Python plug-in that can be used to build project task planning diagram by the terminal command ,  it also can be used to build a mind map.

#### Software Architecture
Software architecture description

#### Installation

1. pip install lmd

2. Installation Environment:

- python >= 3.6

3. Dependency:

- 1. PTable >= 0.9.2

#### Instructions

##### Quick Tutorial

1. Create New projects:

After installing the software, you can create a new project using the bash command with 'lmd' :

`lmd -create 'new project'` or `lmd -c 'new project'`

2.  Show:

Use `lmd -show` or `lmd -s` to view project that is just created:

`lmd -s` or `lmd -show`

You can specify 'id' to specify which project or task need to display using `lmd -s 1`.

```bash
+-------+-------------+----------+----------+---------+
| path  | name        | executor | deadline |  status |
+-------+-------------+----------+----------+---------+
| └── 1 | new project |          |          | working |
+-------+-------------+----------+----------+---------+
```

3. Use `lmd -add` or `lmd -a` commands to add tasks

`lmd -a 1 'task1&&task2'`

- 1 ：project id

- 'task1&&task2': task name

Multiple task names can be separated by an '&&' symbol. A single task does not need an '&&' symbol. You can leave out quotes if there are no Spaces or special characters

```bash

+-----------+-------------+----------+----------+---------+
| path      | name        | executor | deadline |  status |
+-----------+-------------+----------+----------+---------+
| └── 1     | new project |          |          | working |
| │   ├── 2 | task1       |          |          | working |
| │   └── 3 | task2       |          |          | working |
+-----------+-------------+----------+----------+---------+
```

You can also add tasks under task1:

`lmd -a 2 'task3'`

- 2 : the id of 'task1'

- 'task3': task name

```bash
+---------------+-------------+----------+----------+---------+
| path          | name        | executor | deadline |  status |
+---------------+-------------+----------+----------+---------+
| └── 1         | new project |          |          | working |
| │   ├── 2     | task1       |          |          | working |
| │   │   └── 4 | task3       |          |          | working |
| │   └── 3     | task2       |          |          | working |
+---------------+-------------+----------+----------+---------+
```

4.  Add 'executor' to a task using `lmd -executor` or `lmd -exe`

`lmd -executor 2-3 Zhsa`

- 2-3 : task id. The middle sign '-' means' to ', which means' from 2 to 3', and if it's '2-5', it contains the id '2,3,4,5'.

- Zhsa : the name of executor

`lmd -exe 4 Lisi`

```bash
+---------------+-------------+----------+----------+---------+
| path          | name        | executor | deadline |  status |
+---------------+-------------+----------+----------+---------+
| └── 1         | new project |          |          | working |
| │   ├── 2     | task1       |   Zhsa   |          | working |
| │   │   └── 4 | task3       |   Lisi   |          | working |
| │   └── 3     | task2       |   Zhsa   |          | working |
+---------------+-------------+----------+----------+---------+
```

5.  Use `lmd -deadline` or `lmd -dl` to add a 'deadline' to a task.

`lmd -deadline 2-3 20200603`

- 10-11: task id

- 20210603: deadline, suport the date format: yyyymmdd, yyyy/mm/dd, yyyy-mm-dd, yyyy.mm.dd

`lmd -dl 4 2022/07/09`

`lmd -dl 3 '2035.04.08'`

```bash

+---------------+-------------+----------+------------+---------+
| path          | name        | executor |  deadline  |  status |
+---------------+-------------+----------+------------+---------+
| └── 1         | new project |          |            | working |
| │   ├── 2     | task1       |   Zhsa   |  20200603  | working |
| │   │   └── 4 | task3       |   Lisi   | 2022/07/09 | working |
| │   └── 3     | task2       |   Zhsa   | 2035.04.08 | working |
+---------------+-------------+----------+------------+---------+
```

6.  Change the status of tasks

Use `lmd -work`, `lmd -done`, `lmd -cancel`, `lmd -stop` to change the status to 'working', or to 'done', or to 'canceled', or to 'stopped'.

`lmd -done 2`

```bash

+---------------+-------------+----------+------------+---------+
| path          | name        | executor |  deadline  |  status |
+---------------+-------------+----------+------------+---------+
| └── 1         | new project |          |            | working |
| │   ├── 2     | task1       |   Zhsa   |  20200603  |   done  |
| │   │   └── 4 | task3       |   Lisi   | 2022/07/09 |   done  |
| │   └── 3     | task2       |   Zhsa   | 2035.04.08 | working |
+---------------+-------------+----------+------------+---------+
```

> Note: 1. The state change will also change the state of the subtask.

>       2. Update 'finish_date' automatically when the status is changed to done.

>       3. 'finish_date' cleared when status changed to working.


7. Use `lmd -remove` or `lmd -rm` to remove tasks

`lmd -rm 2`

```bash

+-----------+-------------+----------+------------+---------+
| path      | name        | executor |  deadline  |  status |
+-----------+-------------+----------+------------+---------+
| └── 1     | new project |          |            | working |
| │   └── 3 | task2       |   Zhsa   | 2035.04.08 | working |
+-----------+-------------+----------+------------+---------+

```

> Note: 1. Removing tasks and subtasks will be removed as well

8. Use `lmd -s --all` to show all tasks and all information including those that have been removed.

`lmd -s --all`

```bash
+---------------+-------------+----------+-------------+------------+---------+---------+-------------+-------------+
| path          | name        | executor | description |  deadline  |  status | display | finish_date | create_date |
+---------------+-------------+----------+-------------+------------+---------+---------+-------------+-------------+
| └── 1         | new project |          |             |            | working |    1    |             |  2021-06-03 |
| │   ├── 2     | task1       |   Zhsa   |             |  20200603  |   done  |    0    |  2021-06-03 |  2021-06-03 |
| │   │   └── 4 | task3       |   Lisi   |             | 2022/07/09 |   done  |    0    |  2021-06-03 |  2021-06-03 |
| │   └── 3     | task2       |   Zhsa   |             | 2035.04.08 | working |    1    |             |  2021-06-03 |
+---------------+-------------+----------+-------------+------------+---------+---------+-------------+-------------+
```

9. Use `lmd -back` or `lmd -bk` to undo remove operation

`lmd -bk 4`

```bash
+---------------+-------------+----------+------------+---------+
| path          | name        | executor |  deadline  |  status |
+---------------+-------------+----------+------------+---------+
| └── 1         | new project |          |            | working |
| │   ├── 2     | task1       |   Zhsa   |  20200603  |   done  |
| │   │   └── 4 | task3       |   Lisi   | 2022/07/09 |   done  |
| │   └── 3     | task2       |   Zhsa   | 2035.04.08 | working |
+---------------+-------------+----------+------------+---------+
```

> Note: 1. Undoing a removed task will also undo the parent task



10. Use `lmd -description` or `lmd -desc` to add a description of task

`lmd -desc 1 'task have a risk of delay'`

```bash
+---------------+-------------+----------+---------------------------+------------+---------+---------+-------------+-------------+
| path          | name        | executor |        description        |  deadline  |  status | display | finish_date | create_date |
+---------------+-------------+----------+---------------------------+------------+---------+---------+-------------+-------------+
| └── 1         | new project |          | task have a risk of delay |            | working |    1    |             |  2021-06-03 |
| │   ├── 2     | task1       |   Zhsa   |                           |  20200603  |   done  |    1    |  2021-06-03 |  2021-06-03 |
| │   │   └── 4 | task3       |   Lisi   |                           | 2022/07/09 |   done  |    1    |  2021-06-03 |  2021-06-03 |
| │   └── 3     | task2       |   Zhsa   |  some difficult problems  | 2035.04.08 | working |    1    |             |  2021-06-03 |
+---------------+-------------+----------+----------------------------+------------+---------+---------+-------------+-------------+
```

11. Use `lmd -name` or `lmd -na` to Change task name

`lmd -na 3 task4`

```bash
+---------------+-------------+----------+------------+---------+
| path          | name        | executor |  deadline  |  status |
+---------------+-------------+----------+------------+---------+
| └── 1         | new project |          |            | working |
| │   ├── 2     | task1       |   Zhsa   |  20200603  |   done  |
| │   │   └── 4 | task3       |   Lisi   | 2022/07/09 |   done  |
| │   └── 3     | task4       |   Zhsa   | 2035.04.08 | working |
+---------------+-------------+----------+------------+---------+
```

12. Use `lmd -update` or `lmd -u` to change the values of 'name', 'executor', 'description' and 'deadline' simultaneously

`lmd -u 4 'name=task5&&executor=Wawu&&description=perfect&&deadline=2025-12-01'`

或

`lmd -u 4 'na=task5&&exe=Wawu&&desc=perfect&&dl=2025-12-01'`

```bash
+---------------+-------------+----------+---------------------------+------------+---------+---------+-------------+-------------+
| path          | name        | executor |        description        |  deadline  |  status | display | finish_date | create_date |
+---------------+-------------+----------+---------------------------+------------+---------+---------+-------------+-------------+
| └── 1         | new project |          | task have a risk of delay |            | working |    1    |             |  2021-06-03 |
| │   ├── 2     | task1       |   Zhsa   |                           |  20200603  |   done  |    1    |  2021-06-03 |  2021-06-03 |
| │   │   └── 4 | task5       |   Wawu   |          perfect          | 2025-12-01 |   done  |    1    |  2021-06-03 |  2021-06-03 |
| │   └── 3     | task4       |   Zhsa   |  some difficult problems  | 2035.04.08 | working |    1    |             |  2021-06-03 |
+---------------+-------------+----------+---------------------------+------------+---------+---------+-------------+-------------+
```

13. Use `lmd -show --where` to filter the content to display.

`lmd -s --where 'exe=Zhsa'`

```bash
+-----------+-------+----------+------------+---------+
| path      | name  | executor |  deadline  |  status |
+-----------+-------+----------+------------+---------+
| │   ├── 2 | task1 |   Zhsa   |  20200603  |   done  |
| │   └── 3 | task4 |   Zhsa   | 2035.04.08 | working |
+-----------+-------+----------+------------+---------+
```

> Note: 1. Name, Executor, and Description are fuzzy matches. All other fields are exact matches

>       2. Regular matching is not currently supported

>       3. Search for child tasks that are displayed along with the parent task


14. Use `lmd -move` or `lmd -mv` to move a task to another task

`lmd -mv 4 3`

```bash
+---------------+-------------+----------+------------+---------+
| path          | name        | executor |  deadline  |  status |
+---------------+-------------+----------+------------+---------+
| └── 1         | new project |          |            | working |
| │   ├── 2     | task1       |   Zhsa   |  20200603  |   done  |
| │   └── 3     | task4       |   Zhsa   | 2035.04.08 | working |
| │   │   └── 4 | task5       |   Wawu   | 2025-12-01 |   done  |
+---------------+-------------+----------+------------+---------+
```

> Note: Subtasks are carried when a task is moved

15. Use `lmd -delete` or `lmd -d` to permanently delete the task.

`lmd -d 3`

```bash
+-----------+-------------+----------+----------+---------+
| path      | name        | executor | deadline |  status |
+-----------+-------------+----------+----------+---------+
| └── 1     | new project |          |          | working |
| │   └── 2 | task1       |   Zhsa   | 20200603 |   done  |
+-----------+-------------+----------+----------+---------+
```

> Note: 1. Delete will be permanently deleted, and can not be found again, there is no recycle bin, there is no backup.

>       2. Delete carry subtasks, and subtasks are permanently deleted.

Using `lmd -d --all` permanently deletes all projects and tasks

16. Use `lmd -conf show-info` to config the fields to show when use '-show'

`lmd -conf show-info 'path&&name&&deadline&&status'`

```bash
+-----------+-------------+----------+---------+
| path      | name        | deadline |  status |
+-----------+-------------+----------+---------+
| └── 1     | new project |          | working |
| │   └── 2 | task1       | 20200603 |   done  |
+-----------+-------------+----------+---------+
```

`lmd -conf show-info ''` will restore the default values

##### All commands


| COMMANDS           | INSTRUCTIONS                                                            | USAGE                                                                                 |
| :------            | :------                                                                 | :------                                                                               |
| -h,-help           | Show this help message and exit                                         | lmd -h                                                                                |
| -show,-s           | Show project or tasks                                                   | lmd -s [id]                                                                           |
| -all               | Used with '-show' to show all content of task                           | lmd -s --all, lmd -d --all                                                            |
| --where            | Used with '-show' to filter the result                                  | lmd -s [id] --where 'exe=name'                                                        |
| -create,-c         | Create a new project                                                    | lmd -create [project-name]                                                            |
| -add,-a            | Add tasks to a task or project                                          | lmd -add [id] '[task-name]&&[task-name]                                               |
| -remove,-rm        | Make task or tasks not display when execute '-show' command             | lmd -remove [id]/([id]-[id])                                                          |
| -back,-bk          | Make task or tasks display when execute '-show' command                 | lmd -back [id]/([id]-[id])                                                            |
| -delete,-d         | Delete and never find it back                                           | lmd -delete [id]/([id]-[id])                                                          |
| -move,-mv          | Move from one task to another task                                      | lmd -move [id] [father-id]                                                            |
| -name,-na          | Change the name of tasks                                                | lmd -name [id]/([id]-[id]) [new name]                                                 |
| -executor, -exe    | Change the executor of tasks                                            | lmd -executor [id]/([id]-[id]) [new executor]                                         |
| -description,-desc | Change the description of tasks                                         | lmd -desc [id]/([id]-[id]) [new desc]                                                 |
| -deadline,-dl      | Change the deadline of tasks                                            | lmd -dl [id]/([id]-[id]) [new deadline]                                               |
| -update,-u         | Update the fields of task including name,executor,description,deadline  | lmd -update [id] 'executor=[executor]&&description=[description]&&deadline=[deadline] |
| -work              | Turn one task's status to working                                       | lmd -work [id]/([id]-[id])                                                            |
| -done              | Turn one task's status to finish                                        | lmd -done [id]/([id]-[id])                                                            |
| -cancel            | Turn one task's status to canceled                                      | lmd -cancel [id]/([id]-[id])                                                          |
| -stop              | Turn one task's status to stopped                                       | lmd -stop [id]/([id]-[id])                                                            |


#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
