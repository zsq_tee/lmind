.. currentmodule:: lmind

Version 0.1.5
-------------

Released on

-   add ``finish_date`` in --where
-   fix some bug
-   print new task ids when use ``lmd -a`` to add new tasks
-   add '-' in project line

Version 0.1.4
-------------

Released on

-   Not cover the tasks.csv and conf.csv files when upgrate.
-   Fix the bug that can show 10+: ``lmd -show 11`` turn out ``lmd -show 1``.
-   Fix the range show ``lmd -show 1-3``
-   Change the display order of multi ids to show ``lmd -show 10-13``.


Version 0.1.3
-------------

Released on 2021-06-07

-   Add ``lmd -conf show-info`` command to set the fields of show.
-   Add ``conf.csv`` file that storage the config of show-info.
-   Add CHANGES.res file.

