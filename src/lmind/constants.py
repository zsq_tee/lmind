#!/usr/bin/env python
# -*- coding: utf-8 -*-

# status
WORKING = "1"
DONE = "2"
CANCELED = "3"
STOPPED = "4"
STATUS = {
    WORKING: "working",
    DONE: "done",
    CANCELED: "canceled",
    STOPPED: "stopped",
}
