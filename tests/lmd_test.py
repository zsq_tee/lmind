#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import pytest
import json
HOME_PATH = os.path.join(
    os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 
    'src/')
sys.path.append(HOME_PATH)
from lmind.utils import read_csv
from lmind.utils import idx

origs = read_csv()
origs_dict = {i[idx('id')]:i for i in origs}

def test_del_all():
    '''delete all'''
    from lmind.lmd import del_tasks
    res = del_tasks(origs, origs_dict, '1', is_all=True)
    assert res == 1

def test_create():
    '''create project'''
    from lmind.lmd import create
    res = create(origs, 'project')
    assert res == 1

def test_add_tasks():
    from lmind.lmd import add_tasks
    res = add_tasks(origs, '1', ['task2', 'task3'])
    res = add_tasks(origs, '2', ['task4', 'task5'])
    res = add_tasks(origs, '3', ['task6', 'task7'])
    res = add_tasks(origs, '5', ['task8', 'task9'])
    assert res == 1

def test_show():
    '''显示task列表'''
    from lmind.lmd import show
    res = show(origs, origs_dict, '0')
    assert res == 1
    
def test_rm_tasks():
    from lmind.lmd import rm_tasks
    res = rm_tasks(origs, origs_dict, ['4','5'])
    assert res == 1

def test_bk_tasks():
    from lmind.lmd import bk_tasks
    res = bk_tasks(origs, origs_dict, ['4','5'])
    assert res == 1

def test_del_tasks():
    from lmind.lmd import del_tasks
    res = del_tasks(origs, origs_dict, ['4','5'])
    assert res == 1

def test_mv_tasks():
    from lmind.lmd import mv_tasks
    res = mv_tasks(origs_dict, ['3'], '2')
    assert res == 1

def test_update_tasks():
    from lmind.lmd import update_tasks
    res = update_tasks(origs, origs_dict, ['3'] ,dict(executor='张三', description='张三')) 
    assert res == 1

if __name__ == '__main__':
    test_show()
    pass

