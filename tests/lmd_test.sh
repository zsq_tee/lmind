# backup tasks.csv
# if [ -f "tasks.csv" ]; then
#   mv tasks.csv tasks_bak.csv
# fi

# create project
echo "----------> create project <------------"
lmd -create 'new project name'
lmd -s
# cat tasks.csv

# add tasks
echo "----------> add tasks <------------"
lmd -add 1 'task one&&task two&&task three'
lmd -add 3 'task four&&task five&&task six'
lmd -s
# cat tasks.csv

# show
echo "----------> show <------------"
lmd -show 1
lmd -show 3
lmd -show 1 --all
lmd -show 1 --where 'name=task four'

# delete
echo "----------> delete <------------"
lmd -delete 5-6
lmd -s
# cat tasks.csv

# move
echo "----------> move <------------"
lmd -mv 7 1
lmd -s
# cat tasks.csv

# update
echo "----------> update <------------"
lmd -update 7 'executor=zhengshui qing&&desc=zheng shuiqing&&deadline=20210304'
lmd -s
# cat tasks.csv

# name
echo "----------> change name <------------"
lmd -name 7 'new name'
lmd -s
# cat tasks.csv

# executor
echo "----------> change executor <------------"
lmd -executor 7 'new executor'
lmd -s
# cat tasks.csv

# description
echo "----------> change description <------------"
lmd -description 7 'new description'
lmd -s
# cat tasks.csv

# deadline
echo "----------> change deadline <------------"
lmd -deadline 7 '20221202'
lmd -s
# cat tasks.csv

# status
# echo "----------> change status <------------"
# lmd -status 7 '2'
# lmd -s
# cat tasks.csv

# done
echo "----------> change done <------------"
lmd -done 1
lmd -s
# cat tasks.csv


# if [ -f "tasks.csv" ]; then
#   mv tasks.csv tasks_test.csv
# fi
# if [ -f "tasks_bak.csv" ]; then
#   mv tasks_bak.csv tasks.csv
# fi

